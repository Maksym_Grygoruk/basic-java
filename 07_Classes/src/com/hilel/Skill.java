package com.hilel;

public class Skill {
    String name;
    boolean isTechnical;

    public Skill(String name, boolean isTechnical) {
        this.name = name;
        this.isTechnical = isTechnical;
    }
}

